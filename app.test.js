const decodeString = require('./DecodeString');

describe('decodeString', () => {
    test('shouldReturnaIfStringIsa', () => {
        expect(decodeString.Decode('a')).toBe('a');
    });

    test('shouldReturnabIfStringIsab', () => {
        expect(decodeString.Decode('ab')).toBe('ab');
    });

    test('shouldReturnaaaIfStringIs3[a]', () => {
        expect(decodeString.Decode('3[a]')).toBe('aaa');
    });

    test('shouldReturnbcbcIfStringIs2[bc]', () => {
        expect(decodeString.Decode('2[bc]')).toBe('bcbc');
    });

    test('shouldReturnabcbcIfStringIsa2[bc]', () => {
        expect(decodeString.Decode('a2[bc]')).toBe('abcbc');
    });

    test('shouldReturnaaabcbcIfStringIs3[a]2[bc]', () => {
        expect(decodeString.Decode('3[a]2[bc]')).toBe('aaabcbc');
    });
    test('shouldReturnabcabccdcdcdefIfStringIs2[abc]3[cd]ef', () => {
        expect(decodeString.Decode('2[abc]3[cd]ef')).toBe('abcabccdcdcdef');
    });
    test('shouldReturnabccdcdcdxyzIfStringIsabc3[cd]xyz', () => {
        expect(decodeString.Decode('abc3[cd]xyz')).toBe('abccdcdcdxyz');
    });
    test('shouldReturnaccaccaccIfStringIs3[a2[c]]', () => {
        expect(decodeString.Decode('3[a2[c]]')).toBe('accaccacc');
    });

    test('shouldReturnaccccccaccccccaccccccIfStringIs3[a2[3[c]]]', () => {
        expect(decodeString.Decode('3[a2[3[c]]]')).toBe('accccccaccccccacccccc');
    });

    test('shouldReturnaabcabcabcabcabcabcabcabcabcabcabcabcaabcabcabcabcabcabcabcabcabcabcabcabcaabcabcabcabcabcabcabcabcabcabcabcabcIfStringIs3[a2[3[2[abc]]]]', () => {
        expect(decodeString.Decode('3[a2[3[2[abc]]]]')).toBe('aabcabcabcabcabcabcabcabcabcabcabcabcaabcabcabcabcabcabcabcabcabcabcabcabcaabcabcabcabcabcabcabcabcabcabcabcabc');
    });


});


