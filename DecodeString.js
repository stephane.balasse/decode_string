module.exports = class DecodeString {
    static Decode(str){
        let regex = /(?<digit>\d+)\[(?<value>\w+)\]/gm;
        let decodedStr = str.replace(regex, (match, digit, value, offset,stringData) => {
                if(match)
                    return value.repeat(parseInt(digit));
                return stringData;
        });
        return str.indexOf('[') > 0 ? DecodeString.Decode(decodedStr) : decodedStr;
    }
};
